##########  For azure provider ##########

variable "subscription_id" {
  default = "eb0dbdb8-d0d1-4517-bb37-505f8671a3db"
}
variable "tenant_id" {
  default = "1839ece7-7903-4a40-b9c3-2f8146688cf9"
}

########################################

########### Pre-define Vnet and Subnet and NSG #############

variable "predefine_vnet_name" {
  default = "shkp-vn"
}
variable "predefine_vnet_resource_group_name" {
  default = "shkp-vn"
}
variable "predefine_resource_group_name" {
  default = "shkp-pi-roi-uat"
}
variable "predefine_log_analytics_workspace_name" {
  default = "pi-roi-log-analytics-workspace"
}
variable "predefine_log_analytics_workspace_resource_group_name" {
  default = "shkp-pi-roi"
}

#############################################################


################### Project Related Variable #######################

variable "project_name" {
  default = "pi-roi"
}

### if need to append name to all resource, if no need suffix please set default = "" ###
variable "suffix" {
  default = "-uat"
}

variable "management_vm_hostname" {
  default = "uatchstmgmc01"
}

variable "subnet_address_prefixes" {
  default = ["172.23.19.0/25"]
}

variable "agw_subnet_address_prefixes" {
  default  = ["172.23.16.160/28"]
}

variable "agw_private_ip_address" {
  # agw_subnet_address_prefixes + 4 
  default = "172.23.16.164"
}

####################################################################