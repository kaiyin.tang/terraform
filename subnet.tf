locals {
  subnet_name                               = "${var.project_name}${var.suffix}"
  subnet_address_prefixes                   = var.subnet_address_prefixes
  subnet_service_endpoints                  = ["Microsoft.Storage","Microsoft.Sql"]
  subnet_nsg_name                           = "${var.predefine_vnet_name}-${var.project_name}${var.suffix}-nsg"
  agw_subnet_name                           = "${var.project_name}${var.suffix}-agw"
  agw_subnet_address_prefixes               = var.agw_subnet_address_prefixes
  agw_subnet_nsg_name                       = "${var.predefine_vnet_name}-${var.project_name}${var.suffix}-agw-nsg"

  
}

resource "azurerm_network_security_group" "subnet_nsg" {
  name                     = local.subnet_nsg_name
  resource_group_name      = var.predefine_resource_group_name
  location                 = data.azurerm_resource_group.resource_group.location

  #security_rule {
  #  name                       = "test123"
  #  priority                   = 100
  #  direction                  = "Inbound"
  #  access                     = "Allow"
  #  protocol                   = "Tcp"
  #  source_port_range          = "*"
  #  destination_port_range     = "*"
  #  source_address_prefix      = "*"
  #  destination_address_prefix = "*"
  #}

  tags = {
    applicationType = var.project_name
  }
}
resource "azurerm_network_security_group" "agw_subnet_nsg" {
  name                     = local.agw_subnet_nsg_name
  resource_group_name      = var.predefine_resource_group_name
  location                 = data.azurerm_resource_group.resource_group.location

  security_rule {
    name                       = "Allow_GWM"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "65200-65535"
    destination_port_range     = "65200-65535"
    source_address_prefix      = "GatewayManager"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "Allow_HTTP"
    priority                   = 110
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "Allow_HTTPS"
    priority                   = 120
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "443"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "Allow_ACME_HTTP-01"
    priority                   = 130
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "8089"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "Allow_AGW_HealthProbe"
    priority                   = 150
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "65503-65534"
    source_address_prefix      = "Internet"
    destination_address_prefix = "*"
  }

  tags = {
    applicationType = var.project_name
  }
}

resource "azurerm_subnet" "subnet" {
  name                                            = local.subnet_name
  resource_group_name                             = var.predefine_vnet_resource_group_name
  virtual_network_name                            = var.predefine_vnet_name
  address_prefixes                                = local.subnet_address_prefixes
  enforce_private_link_endpoint_network_policies  = true
  enforce_private_link_service_network_policies   = false
  service_endpoints                               = local.subnet_service_endpoints  
}


resource "azurerm_subnet" "agw_subnet" {
  name                                            = local.agw_subnet_name
  resource_group_name                             = var.predefine_vnet_resource_group_name
  virtual_network_name                            = var.predefine_vnet_name
  address_prefixes                                = local.agw_subnet_address_prefixes
}

resource "azurerm_subnet_network_security_group_association" "subnet_nsg_asso" {
  subnet_id                 = azurerm_subnet.subnet.id
  network_security_group_id = azurerm_network_security_group.subnet_nsg.id
}

resource "azurerm_subnet_network_security_group_association" "agw_subnet_nsg_asso" {
  subnet_id                 = azurerm_subnet.agw_subnet.id
  network_security_group_id = azurerm_network_security_group.agw_subnet_nsg.id
}