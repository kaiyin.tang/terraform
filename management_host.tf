locals {
  network_interface_name                     = "${var.management_vm_hostname}-nic"
  vm_name                                    = var.management_vm_hostname
  vm_size                                    = "Standard_B4ms"
  vm_username                                = "techadmin"
  vm_pass                                    = "L0calpa552018"
  vm_source_image = {
    publisher                                = "OpenLogic"
    offer                                    = "CentOS"
    sku                                      = "8_2-gen2"
    version                                  = "latest"
  }
  vm_os_disk = {
    caching                                  = "ReadWrite"
    storage_account_type                     = "StandardSSD_LRS"
  }                              
}

resource "azurerm_network_interface" "mgmt_vm_nic" {
  name                = local.network_interface_name
  resource_group_name      = var.predefine_resource_group_name
  location                 = data.azurerm_resource_group.resource_group.location
  tags = {
    applicationType = var.project_name
  }

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.subnet.id
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_linux_virtual_machine" "mgmt_vm" {
  name                                = local.vm_name
  resource_group_name                 = var.predefine_resource_group_name
  location                            = data.azurerm_resource_group.resource_group.location
  size                                = local.vm_size
  admin_username                      = local.vm_username
  admin_password                      = local.vm_pass
  disable_password_authentication     = false
  network_interface_ids = [
    azurerm_network_interface.mgmt_vm_nic.id,
  ]

  tags = {
    applicationType = var.project_name
  }

  os_disk {
    caching              = local.vm_os_disk.caching
    storage_account_type = local.vm_os_disk.storage_account_type
  }
  # run "az vm image list --offer CentOS --all --output table" to get avaliable sku and version
  source_image_reference {
    publisher = local.vm_source_image.publisher
    offer     = local.vm_source_image.offer
    sku       = local.vm_source_image.sku
    version   = local.vm_source_image.version
  }
}


output "mgmt_vm_private_ip_address" {
  value = azurerm_linux_virtual_machine.mgmt_vm.private_ip_address
}