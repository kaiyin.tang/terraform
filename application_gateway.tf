locals {
  agw_pip_name                               = "${var.project_name}${var.suffix}-agw-pip"
  agw_name                                   = "${var.project_name}${var.suffix}-agw"
  agw_sku_name                               = "WAF_v2"
  agw_sku_tier                               = "WAF_v2"
  agw_auto_apacity_min                       = 0
  agw_auto_capacity_max                      = 10
  # agw_enable_http2 should be true/false
  agw_enable_http2                           = false
  agw_gateway_ip_configuration_name          = "${var.project_name}${var.suffix}-agw-gwip"
  agw_frontend_ip_configuration_name         = "${var.project_name}${var.suffix}-agw-feip"
  agw_private_ip_address                     = var.agw_private_ip_address
  agw_frontend_port_name                     = "${var.project_name}${var.suffix}-agw-feport"
  agw_backend_address_pool_name              = "${var.project_name}${var.suffix}-agw-beap"  
  agw_backend_http_setting_name              = "${var.project_name}${var.suffix}-agw-be-htst"
  agw_http_listener_name                     = "${var.project_name}${var.suffix}-agw-httplstn"
  agw_request_routing_rule_name              = "${var.project_name}${var.suffix}-agw-rqrt"
  agw_redirect_configuration_name            = "${var.project_name}${var.suffix}-agw-rdrcfg"
  agw_probe_name                             = "${var.project_name}${var.suffix}-agw-probe"
  agw_monitor_diagnostic_setting_name        = "${var.project_name}${var.suffix}-agw-log-diag"
  waf_name                                   = "${var.project_name}${var.suffix}-waf"
  # waf_enable should be true/false
  waf_enable                                 = true
  waf_configuration                          = {
    file_upload_limit_mb                     = 100
    max_request_body_size_kb                 = 128
    firewall_mode                            = "Detection"
    rule_set_type                            = "OWASP"
    rule_set_version                         = "3.1"
  }
}


resource "azurerm_public_ip" "agw_pip" {
  name                = local.agw_pip_name
  resource_group_name = var.predefine_resource_group_name
  location            = data.azurerm_resource_group.resource_group.location
  allocation_method   = "Static"
  sku                 = "Standard"
  tags = {
    applicationType = var.project_name
  }
}



resource "azurerm_application_gateway" "agw" {
  name                = local.agw_name
  resource_group_name = var.predefine_resource_group_name
  location            = data.azurerm_resource_group.resource_group.location
  enable_http2        = local.agw_enable_http2
  #zones               = var.zones
  firewall_policy_id  = azurerm_web_application_firewall_policy.waf.id
  tags = {
    applicationType = var.project_name
  }

  sku {
    name     = local.agw_sku_name
    tier     = local.agw_sku_tier
  }
  autoscale_configuration {
    min_capacity = local.agw_auto_apacity_min
    max_capacity = local.agw_auto_capacity_max
  }

  gateway_ip_configuration {
    name      = local.agw_gateway_ip_configuration_name
    subnet_id = azurerm_subnet.agw_subnet.id
  }

  frontend_ip_configuration {
    name                 = "${local.agw_frontend_ip_configuration_name}-public"
    public_ip_address_id = azurerm_public_ip.agw_pip.id
  }

  frontend_ip_configuration {
    name                          = "${local.agw_frontend_ip_configuration_name}-private"
    private_ip_address_allocation = "Static"
    private_ip_address            = local.agw_private_ip_address
    subnet_id                     = azurerm_subnet.agw_subnet.id
  }

  frontend_port {
    name = "${local.agw_frontend_port_name}-80"
    port = 80
  }

  frontend_port {
    name = "${local.agw_frontend_port_name}-443"
    port = 443
  }

  backend_address_pool {
    name = local.agw_backend_address_pool_name
  }

  backend_http_settings {
    name                  = local.agw_backend_http_setting_name
    cookie_based_affinity = "Disabled"
    path                  = "/"
    port                  = 80
    protocol              = "Http"
    request_timeout       = 30
    probe_name            = "${local.agw_probe_name}-HTTP"
  }

  http_listener {
    name                           = local.agw_http_listener_name
    frontend_ip_configuration_name = "${local.agw_frontend_ip_configuration_name}-public"
    frontend_port_name             = "${local.agw_frontend_port_name}-80"
    protocol                       = "Http"
  }

  request_routing_rule {
    name                       = local.agw_request_routing_rule_name
    rule_type                  = "Basic"
    http_listener_name         = local.agw_http_listener_name
    backend_address_pool_name  = local.agw_backend_address_pool_name
    backend_http_settings_name = local.agw_backend_http_setting_name
  }
  
  probe {
    name                                        = "${local.agw_probe_name}-HTTP"
    pick_host_name_from_backend_http_settings   = false
    host                                        = "localhost"
    interval                                    = 30
    protocol                                    = "Http"
    path                                        = "/"
    timeout                                     = 30
    unhealthy_threshold                         = 3
  }
  probe {
    name                                        = "${local.agw_probe_name}-HTTPS"
    pick_host_name_from_backend_http_settings   = false
    host                                        = "localhost"
    interval                                    = 30
    protocol                                    = "Https"
    path                                        = "/"
    timeout                                     = 30
    unhealthy_threshold                         = 3
  }

  #ssl_policy {
  #  policy_type = "Predefined"
  #  policy_name = var.ssl_policy_name
  #}

  #waf_configuration {
  #  enabled                  = local.waf_enabled
  #  firewall_mode            = coalesce(local.waf_configuration != null ? local.waf_configuration.firewall_mode : null, "Detection")
  #  rule_set_type            = coalesce(local.waf_configuration != null ? local.waf_configuration.rule_set_type : null, "OWASP")
  #  rule_set_version         = coalesce(local.waf_configuration != null ? local.waf_configuration.rule_set_version : null, "3.0")
  #  file_upload_limit_mb     = coalesce(local.waf_configuration != null ? local.waf_configuration.file_upload_limit_mb : null, 100)
  #  max_request_body_size_kb = coalesce(local.waf_configuration != null ? local.waf_configuration.max_request_body_size_kb : null, 128)
  #}

  #dynamic "custom_error_configuration" {
  #  for_each = var.custom_error
  #  iterator = ce
  #  content {
  #    status_code           = ce.value.status_code
  #    custom_error_page_url = ce.value.error_page_url
  #  }
  #}

  // Ignore most changes as they should be managed by AKS ingress controller
  lifecycle {
    ignore_changes = [
      backend_address_pool,
      backend_http_settings,
      frontend_port,
      http_listener,
      probe,
      request_routing_rule,
      url_path_map,
      ssl_certificate,
      redirect_configuration,
      autoscale_configuration,
      tags["managed-by-k8s-ingress"],
      tags["last-updated-by-k8s-ingress"],
    ]
  }
}


resource "azurerm_web_application_firewall_policy" "waf" {
  name                = local.waf_name
  resource_group_name = var.predefine_resource_group_name
  location            = data.azurerm_resource_group.resource_group.location

  tags = {
    applicationType = var.project_name
  }

  policy_settings {
    enabled                     = local.waf_enable
    file_upload_limit_in_mb     = coalesce(local.waf_configuration != null ? local.waf_configuration.file_upload_limit_mb : null, 100)
    max_request_body_size_in_kb = coalesce(local.waf_configuration != null ? local.waf_configuration.max_request_body_size_kb : null, 128)
    mode                        = coalesce(local.waf_configuration != null ? local.waf_configuration.firewall_mode : null, "Prevention")
    request_body_check          = true
  }

  #dynamic "custom_rules" {
  #  for_each = var.custom_policies
  #  iterator = cp
  #  content {
  #    name      = cp.value.name
  #    priority  = (cp.key + 1) * 10
  #    rule_type = cp.value.rule_type
  #    action    = cp.value.action
  #
  #    dynamic "match_conditions" {
  #      for_each = cp.value.match_conditions
  #      iterator = mc
  #      content {
  #        dynamic "match_variables" {
  #          for_each = mc.value.match_variables
  #          iterator = mv
  #          content {
  #            variable_name = mv.value.match_variable
  #            selector      = mv.value.selector
  #          }
  #        }
  #
  #        operator           = mc.value.operator
  #        negation_condition = mc.value.negation_condition
  #        match_values       = mc.value.match_values
  #      }
  #    }
  #  }
  #}

  managed_rules {
    managed_rule_set {
      type    = coalesce(local.waf_configuration != null ? local.waf_configuration.rule_set_type : null, "OWASP")
      version = coalesce(local.waf_configuration != null ? local.waf_configuration.rule_set_version : null, "3.1")

      #dynamic "rule_group_override" {
      #  for_each = var.managed_policies_override
      #  iterator = rg
      #  content {
      #    rule_group_name = rg.value.rule_group_name
      #    disabled_rules  = rg.value.disabled_rules
      #  }
      #}
    }

    #dynamic "exclusion" {
    #  for_each = var.managed_policies_exclusions
    #  iterator = ex
    #  content {
    #    match_variable          = ex.value.match_variable
    #    selector                = ex.value.selector
    #    selector_match_operator = ex.value.selector_match_operator
    #  }
    #}
  }
}



resource "azurerm_monitor_diagnostic_setting" "agw_monitor_diagnostic_setting" {
  name                           = local.agw_monitor_diagnostic_setting_name
  target_resource_id             = azurerm_application_gateway.agw.id
  log_analytics_workspace_id     = data.azurerm_log_analytics_workspace.log_analytics_workspace.id
  #eventhub_authorization_rule_id = local.parsed_diag.event_hub_auth_id
  #eventhub_name                  = local.parsed_diag.event_hub_auth_id != null ? var.diagnostics.eventhub_name_log : null
  #storage_account_id             = local.parsed_diag.storage_account_id

  log {
    category = "ApplicationGatewayAccessLog"
    
    retention_policy {
      enabled = false
    }
  }
  log {
    category = "ApplicationGatewayPerformanceLog"
    
    retention_policy {
      enabled = false
    }
  }
  log {
    category = "ApplicationGatewayFirewallLog"
    
    retention_policy {
      enabled = false
    }
  }

  metric {
    category = "AllMetrics"

    retention_policy {
      enabled = false
    }
  }
}
