data "azurerm_virtual_network" "vnet" {
  name                = var.predefine_vnet_name
  resource_group_name = var.predefine_vnet_resource_group_name
}

output "vnet_id" {
  value = data.azurerm_virtual_network.vnet.id
}
output "vnet_location" {
  value = data.azurerm_virtual_network.vnet.location
}
output "vnet_name" {
  value = data.azurerm_virtual_network.vnet.name
}

data "azurerm_resource_group" "resource_group" {
  name = var.predefine_resource_group_name
}

output "resource_group_id" {
  value = data.azurerm_resource_group.resource_group.id
}

output "resource_group_location" {
  value = data.azurerm_resource_group.resource_group.location
}


data "azurerm_log_analytics_workspace" "log_analytics_workspace" {
  name                = var.predefine_log_analytics_workspace_name
  resource_group_name = var.predefine_log_analytics_workspace_resource_group_name
}

output "log_analytics_workspace_id" {
  value = data.azurerm_log_analytics_workspace.log_analytics_workspace.workspace_id
}