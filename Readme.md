# Initial setup

1. install terraform ( https://www.terraform.io/downloads.html )
2. install azure cli ( https://docs.microsoft.com/en-us/cli/azure/install-azure-cli )
3. run `az login` to login
4. It will show your subscription info after login, copy the subscription ID and tenant ID
``` json
[
  {
    "cloudName": "AzureCloud",
    "homeTenantId": "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
    "id": "<  this is subscription ID  >",
    "isDefault": true,
    "managedByTenants": [],
    "name": "some subscription name",
    "state": "Enabled",
    "tenantId": "<  this is tenant ID  >",
    "user": {
      "name": "some user name",
      "type": "user"
    }
  }
]

```

# Config Terraform before execute

1. edit `variables.tf`
2. fill in subscription ID and tenant ID which got in initial setup
``` tf
##########  For azure provider ##########

variable "subscription_id" {
  default = "xxxxxxxxxxxxxxxxxxxxxxxxxxxx"
}
variable "tenant_id" {
  default = "xxxxxxxxxxxxxxxxxxxxxxxxxxxx"
}

########################################
```
3. fill in the rest of parameter (`must be filled in`) in `variables.tf`

# Terraform execute

1. if first time to use terraform you need to run , it will help you to install the provider 
```bash
terraform init
```

2. check syntax of script before execute
```bash
terraform plan
```

3. execute script to create all resource
```bash
terraform apply
```
4. remove all resource
```bash
terraform destroy
```
5. remove target resource
```bash
terraform destroy -target <resource name>
```
for example I want to remove the application gateway, from `application_gateway.tf` find the resource block:
``` tf
resource "azurerm_application_gateway" "agw" {
  name                = local.agw_name
  resource_group_name = var.predefine_resource_group_name
  location            = data.azurerm_resource_group.resource_group.location
  ....

}
```
the `<resource name>` is `azurerm_application_gateway.agw`

6. Return the result output (the output have info of the resources)
```bash
terraform output

# you can save it by
terraform output > output.txt
```