locals {
    aks_name                                  = "${var.project_name}${var.suffix}-aks"
    aks_version                               = "1.18.14"
    aks_container_insight_enalbed             = true
    aks_nodepool = {
      enable_auto_scaling                     = true
      node_pool_name                          = "workernodes"
      node_count                              = 1
      vm_size                                 = "Standard_D4_v3"
      max_count                               = 3
      min_count                               = 1
    }
    aks_networking = {
      network_plugin                          = "kubenet"
      private_cluster_enabled                 = false
      pod_cidr                                = "10.244.0.0/16"
      service_cidr                            = "10.0.0.0/16"
      dns_service_ip                          = "10.0.0.10"
      docker_bridge_cidr                      = "172.1.0.1/16"
      http_application_routing_enabled        = false
    }
    aks_monitor_diagnostic_setting_name       = "${var.project_name}${var.suffix}-aks-log-diag"
}

resource "azurerm_kubernetes_cluster" "aks" {
  name                     = local.aks_name
  resource_group_name      = var.predefine_resource_group_name
  location                 = data.azurerm_resource_group.resource_group.location
  dns_prefix               = local.aks_name  
  private_cluster_enabled  = local.aks_networking.private_cluster_enabled
  kubernetes_version       = local.aks_version

  default_node_pool {
    name                   = local.aks_nodepool.node_pool_name
    node_count             = local.aks_nodepool.node_count
    vm_size                = local.aks_nodepool.vm_size
    max_count              = local.aks_nodepool.max_count
    min_count              = local.aks_nodepool.min_count
    enable_auto_scaling    = local.aks_nodepool.enable_auto_scaling
    vnet_subnet_id         = azurerm_subnet.subnet.id
  }
  
  identity {
    type = "SystemAssigned"
  }

  addon_profile {
    http_application_routing {
      enabled = local.aks_networking.http_application_routing_enabled
    }
    oms_agent {
      enabled                    =  local.aks_container_insight_enalbed
      log_analytics_workspace_id =  data.azurerm_log_analytics_workspace.log_analytics_workspace.id
    }  
  }


  network_profile {
    network_plugin     = local.aks_networking.network_plugin
    dns_service_ip     = local.aks_networking.dns_service_ip
    docker_bridge_cidr = local.aks_networking.docker_bridge_cidr
    service_cidr       = local.aks_networking.service_cidr
    pod_cidr           = local.aks_networking.pod_cidr
  }
  tags = {
    applicationType = var.project_name
  }

}

resource "null_resource" "az_exec_aks_enable_addons_agw" {
  provisioner "local-exec" {
    command = "az aks enable-addons --resource-group  ${var.predefine_resource_group_name} --name ${local.aks_name} --addons ingress-appgw --appgw-id ${azurerm_application_gateway.agw.id}"
    
  }
  depends_on = [
    azurerm_kubernetes_cluster.aks,
    azurerm_application_gateway.agw
  ]
}
resource "null_resource" "az_exec_update_agw_to_use_aks_route_table" {
  provisioner "local-exec" {
    interpreter = ["powershell" ,"-c"]
    command = <<EOT
      $routeTableId=$(az network route-table list -g ${azurerm_kubernetes_cluster.aks.node_resource_group} --query "[].id | [0]" -o tsv)
      az network vnet subnet update --ids ${azurerm_subnet.agw_subnet.id} --route-table $routeTableId
    EOT
    
  }
  depends_on = [
    null_resource.az_exec_aks_enable_addons_agw
  ]
}


resource "azurerm_monitor_diagnostic_setting" "aks_monitor_diagnostic_setting" {
  name                           = local.aks_monitor_diagnostic_setting_name
  target_resource_id             = azurerm_kubernetes_cluster.aks.id
  log_analytics_workspace_id     = data.azurerm_log_analytics_workspace.log_analytics_workspace.id


  log {
    category = "kube-apiserver"
    
    retention_policy {
      enabled = false
    }
  }
  log {
    category = "kube-audit"
    
    retention_policy {
      enabled = false
    }
  }
  log {
    category = "kube-audit-admin"
    
    retention_policy {
      enabled = false
    }
  }
  log {
    category = "kube-controller-manager"
    
    retention_policy {
      enabled = false
    }
  }

  log {
    category = "kube-scheduler"
    
    retention_policy {
      enabled = false
    }
  }
  
  log {
    category = "cluster-autoscaler"
    
    retention_policy {
      enabled = false
    }
  }
  log {
    category = "guard"
    
    retention_policy {
      enabled = false
    }
  }



  metric {
    category = "AllMetrics"

    retention_policy {
      enabled = false
    }
  }
}

output "aks_client_certificate" {
  value = azurerm_kubernetes_cluster.aks.kube_config.0.client_certificate
}

output "aks_kube_config" {
  value = azurerm_kubernetes_cluster.aks.kube_config_raw
}
output "aks_node_resource_group" {
  value = azurerm_kubernetes_cluster.aks.node_resource_group
}
