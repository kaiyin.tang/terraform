locals {
  postgresql_server_name                      = "${var.project_name}${var.suffix}-postgresql"
  postgresql_server_config = {
    administrator_login                       = "pgadmin"
    administrator_login_password              = "Pg_pwd2021"
    sku_name                                  = "MO_Gen5_2"
    version                                   = 11
    storage_mb                                = 102400
    backup_retention_days                     = 7
    geo_redundant_backup_enabled              = true
    auto_grow_enabled                         = true
    public_network_access_enabled             = false
    ssl_enforcement_enabled                   = true
    ssl_minimal_tls_version_enforced          = "TLS1_2"
  }
  postgresql_server_virtual_network_rule_name = "${var.project_name}${var.suffix}-postgresql-vnet-rule"
  postgresql_pe_name                                     = "${var.project_name}${var.suffix}-pe-postgresql"
}

resource "azurerm_postgresql_server" "postgresql" {
  name                              = local.postgresql_server_name
  resource_group_name               = var.predefine_resource_group_name
  location                          = data.azurerm_resource_group.resource_group.location


  administrator_login               = local.postgresql_server_config.administrator_login
  administrator_login_password      = local.postgresql_server_config.administrator_login_password

  sku_name                          = local.postgresql_server_config.sku_name
  version                           = local.postgresql_server_config.version
  storage_mb                        = local.postgresql_server_config.storage_mb

  backup_retention_days             = local.postgresql_server_config.backup_retention_days
  geo_redundant_backup_enabled      = local.postgresql_server_config.geo_redundant_backup_enabled
  auto_grow_enabled                 = local.postgresql_server_config.auto_grow_enabled

  public_network_access_enabled     = local.postgresql_server_config.public_network_access_enabled
  ssl_enforcement_enabled           = local.postgresql_server_config.ssl_enforcement_enabled
  ssl_minimal_tls_version_enforced  = local.postgresql_server_config.ssl_minimal_tls_version_enforced

  tags = {
    applicationType = var.project_name
  }
}

# not work
#resource "azurerm_postgresql_virtual_network_rule" "postgresql_virtual_network_rule" {
#  name                                 = local.postgresql_server_virtual_network_rule_name
#  resource_group_name                  = var.predefine_resource_group_name
#  server_name                          = azurerm_postgresql_server.postgresql.name
#  subnet_id                            = azurerm_subnet.subnet.id
#  ignore_missing_vnet_service_endpoint = true
#}

resource "azurerm_private_endpoint" "postgresql_pe" {
  name                = local.postgresql_pe_name
  resource_group_name = var.predefine_resource_group_name
  location            = data.azurerm_resource_group.resource_group.location
  subnet_id           = azurerm_subnet.subnet.id
  tags = {
    applicationType = var.project_name
  }

  private_service_connection {
    name                           = "${local.postgresql_server_name}-pe-connection"
    private_connection_resource_id = azurerm_postgresql_server.postgresql.id
    subresource_names              = ["postgresqlServer"]
    is_manual_connection           = false
  }
}


output "postgresql_server_fqdn" {
  value = azurerm_postgresql_server.postgresql.fqdn
}
output "private_endpoint_postgresql_custom_dns_configs" {
  value = azurerm_private_endpoint.postgresql_pe.custom_dns_configs
}

