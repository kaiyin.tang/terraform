locals {
  storage_account_name = replace("${var.project_name}${var.suffix}-sa","-","")
  storage_account_pe_name              = "${var.project_name}${var.suffix}-pe-sa"
}

resource "azurerm_storage_account" "storage_account" {
  name                     = local.storage_account_name
  resource_group_name      = var.predefine_resource_group_name
  location                 = data.azurerm_resource_group.resource_group.location
  account_tier             = "Standard"
  account_replication_type = "GRS"

  tags = {
    applicationType = var.project_name
  }
}


resource "azurerm_private_endpoint" "storage_account_pe" {
  name                = local.storage_account_pe_name
  resource_group_name = var.predefine_resource_group_name
  location            = data.azurerm_resource_group.resource_group.location
  subnet_id           = azurerm_subnet.subnet.id
  tags = {
    applicationType = var.project_name
  }

  private_service_connection {
    name                           = "${azurerm_storage_account.storage_account.name}-pe-connection"
    is_manual_connection           = false
    private_connection_resource_id = azurerm_storage_account.storage_account.id
    subresource_names              = ["file"]
  }
  
}

output "private_endpoint_storage_account_custom_dns_configs" {
  value = azurerm_private_endpoint.storage_account_pe.custom_dns_configs
}